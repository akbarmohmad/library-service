package com.jpop.libraryservice.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
@ApiModel
public class BookDto {

    private static final long serialVersionUID = 1L;

    private Long bookId;

    
    @ApiModelProperty(notes = "Book title", name="title", required = true)
    @NotBlank
    private String title;

    @ApiModelProperty(notes = "Book author", name="author", required = true)
    @NotBlank
    private String author;

    @ApiModelProperty(notes = "Book price", name="price", required = true)
    @NotNull
    private Double price;

    @ApiModelProperty(notes = "Book publishedYear", name="publishedYear", required = true)
    @NotBlank
    private String publishedYear;


    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getPublishedYear() {
        return publishedYear;
    }

    public void setPublishedYear(String publishedYear) {
        this.publishedYear = publishedYear;
    }
}
