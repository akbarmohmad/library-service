package com.jpop.libraryservice.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;

@ApiModel
public class UserDto {

    private Long userId;
    @ApiModelProperty(notes = "User FirstName", name="firstName", required = true)
    @NotBlank
    private String firstName;

    @ApiModelProperty(notes = "User MiddleName", name="middleName")
    private String middleName;

    @ApiModelProperty(notes = "User LastName", name="lastName", required = true)
    @NotBlank
    private String lastName;

    @ApiModelProperty(notes = "UserName", name="userName", required = true)
    private String userName;

    @ApiModelProperty(notes = "User Email", name="email", required = true)
    @NotBlank
    private String email;

    @ApiModelProperty(notes = "User PhoneNumber", name="contact", required = true)
    @NotBlank
    private String contact;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

}
