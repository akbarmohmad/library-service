package com.jpop.libraryservice.client.book;

import com.jpop.libraryservice.dto.BookDto;

import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;
@Named
public class BookServiceClientFallBack implements BookServiceClient {
    @Override
    public List<BookDto> getBooks() {
        List<BookDto> books = new ArrayList<>();
        books.add(getBookDto(0L));
        return books;
    }

    private BookDto getBookDto(Long bookId) {
        BookDto book = new BookDto();
        book.setBookId(bookId);
        book.setAuthor("Dummy Book Author");
        book.setTitle("Dummy Book");
        book.setPublishedYear("1900");
        book.setPrice(0.0);
        return book;
    }

    @Override
    public BookDto getBook(Long bookId) {
        return getBookDto(bookId);
    }

    @Override
    public BookDto addBook(BookDto book) {
        return getBookDto(0L);
    }

    @Override
    public BookDto updateBook(Long bookId, BookDto bookDetails) {
        return getBookDto(bookId);
    }

    @Override
    public void deleteBook(Long bookId) {

    }
}
