package com.jpop.libraryservice.client.book;

import com.jpop.libraryservice.dto.BookDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name="${book.service.appId}", fallback = BookServiceClientFallBack.class)
public interface BookServiceClient {

    @GetMapping(value="/books")
    List<BookDto> getBooks();

    @GetMapping(value="/books/{book_id}")
    BookDto getBook(@PathVariable(value = "book_id") Long bookId);

    @PostMapping(value="/books")
    BookDto addBook(@RequestBody BookDto book);

    @PutMapping(value="/books/{book_id}")
    BookDto updateBook(@PathVariable(value = "book_id") Long bookId, @RequestBody BookDto bookDetails);

    @DeleteMapping(value="/books/{book_id}")
    void deleteBook(@PathVariable(value = "book_id") Long bookId);
}
