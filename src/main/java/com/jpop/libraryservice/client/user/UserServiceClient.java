package com.jpop.libraryservice.client.user;

import com.jpop.libraryservice.client.book.BookServiceClientFallBack;
import com.jpop.libraryservice.dto.UserDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name="${user.service.appId}", fallback = UserServiceClientFallBack.class)
public interface UserServiceClient {

    @GetMapping(value="/users")
    ResponseEntity<List<UserDto>> getUsers();

    @GetMapping(value="/users/{user_id}")
    ResponseEntity<UserDto> getUser(@PathVariable(value = "user_id") Long userId);

    @PostMapping(value="/users")
    ResponseEntity<UserDto> addUser(@RequestBody UserDto UserDto);

    @PutMapping(value="/users/{user_id}")
    ResponseEntity<UserDto> updateUser(@PathVariable(value = "user_id") Long userId, @RequestBody UserDto userDetails);

    @DeleteMapping(value="/users/{user_id}")
    ResponseEntity deleteUser(@PathVariable(value = "user_id") Long userId);
}
