package com.jpop.libraryservice.client.user;

import com.jpop.libraryservice.dto.UserDto;
import org.springframework.http.ResponseEntity;

import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;
@Named
public class UserServiceClientFallBack implements UserServiceClient {
    @Override
    public ResponseEntity<List<UserDto>> getUsers() {
        List<UserDto> users = new ArrayList<>();
        users.add(getUserDto(0L));
        return ResponseEntity.ok(users);
    }

    private UserDto getUserDto(Long id) {
        UserDto user = new UserDto();
        user.setUserId(id);
        user.setEmail("Dummy Email");
        user.setFirstName("Dummy FirstName");
        user.setLastName("Dummy LastName");
        user.setContact("897576757667");
        user.setUserName("Dummy UserName");
        return user;
    }

    @Override
    public ResponseEntity<UserDto> getUser(Long userId) {
        return ResponseEntity.ok(getUserDto(userId));
    }

    @Override
    public ResponseEntity<UserDto> addUser(UserDto UserDto) {
        return ResponseEntity.ok(getUserDto(0L));
    }

    @Override
    public ResponseEntity<UserDto> updateUser(Long userId, UserDto userDetails) {
        return ResponseEntity.ok(getUserDto(userId));
    }

    @Override
    public ResponseEntity deleteUser(Long userId) {
        return ResponseEntity.ok().build();
    }
}
