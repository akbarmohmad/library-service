package com.jpop.libraryservice.resource.impl;

import com.jpop.libraryservice.dto.BookDto;
import com.jpop.libraryservice.dto.UserDto;
import com.jpop.libraryservice.exception.LibraryServiceException;
import com.jpop.libraryservice.model.Library;
import com.jpop.libraryservice.repository.LibraryRepository;
import com.jpop.libraryservice.resource.LibraryResource;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.Valid;
import java.util.List;

@Named
public class LibraryResourceImpl implements LibraryResource {

    @Inject
    private RestTemplate restTemplate;

    @Inject
    private LibraryRepository libraryRepository;

    @Inject
    private EurekaClient eurekaClient;

    @Value("${user.service.appId}")
    private String userServiceAppId;

    @Value("${book.service.appId}")
    private String bookServiceAppId;

    private String getBookServiceUrl(){
        return getBaseUrl(bookServiceAppId)+"/books/";
    }

    private String getUserServiceUrl(){
        return getBaseUrl(userServiceAppId)+"/users/";
    }
    private String getBaseUrl(String appId) {
        InstanceInfo instanceInfo = eurekaClient.getNextServerFromEureka(appId, false);
        return instanceInfo.getHomePageUrl();
    }


    @Override
    public ResponseEntity<List<BookDto>> getBooks() {
        ResponseEntity<List<BookDto>> responseEntity = restTemplate.exchange(getBookServiceUrl(), HttpMethod.GET, null, new ParameterizedTypeReference<List<BookDto>>() {
        });
        return verifyResponse(responseEntity, "getBooks");
    }

    private <T> ResponseEntity<T> verifyResponse(ResponseEntity<T> responseEntity, String param) {
        if (responseEntity.getStatusCode() == HttpStatus.OK) {
            return responseEntity;
        }
        throw new LibraryServiceException("Failed to " + param);
    }

    @Override
    public ResponseEntity<BookDto> getBook(Long bookId) {
        ResponseEntity<BookDto> responseEntity = restTemplate.getForEntity(getBookServiceUrl() + bookId, BookDto.class);
        return verifyResponse(responseEntity, "getBook");
    }

    @Override
    public ResponseEntity<BookDto> addBook(BookDto book) {
        ResponseEntity<BookDto> responseEntity = restTemplate.postForEntity(getBookServiceUrl(), book, BookDto.class);
        return verifyResponse(responseEntity, "addBook");
    }

    @Override
    public ResponseEntity deleteBook(Long bookId) {
        ResponseEntity<ResponseEntity> response = restTemplate.exchange(getBookServiceUrl() + bookId, HttpMethod.DELETE, null, ResponseEntity.class);
        return verifyResponse(response, "deleteBook");
    }

    @Override
    public ResponseEntity<List<UserDto>> getUsers() {
        ResponseEntity<List<UserDto>> response = restTemplate.exchange( getUserServiceUrl(), HttpMethod.GET, null, new ParameterizedTypeReference<List<UserDto>>() {
        });
        return verifyResponse(response, "getUsers");
    }

    @Override
    public ResponseEntity<UserDto> getUser(Long userId) {
        ResponseEntity<UserDto> response = restTemplate.getForEntity( getUserServiceUrl() + userId, UserDto.class);
        return verifyResponse(response, "getUser");
    }

    @Override
    public ResponseEntity<UserDto> addUser(@Valid UserDto userDto) {
        ResponseEntity<UserDto> user = restTemplate.postForEntity( getUserServiceUrl(), userDto, UserDto.class);
        return verifyResponse(user, "addUser");
    }

    @Override
    public ResponseEntity deleteUser(Long userId) {
        ResponseEntity<ResponseEntity> response = restTemplate.exchange( getUserServiceUrl() + userId, HttpMethod.DELETE, null, ResponseEntity.class);
        return verifyResponse(response, "deleteUser");
    }

    @Override
    public ResponseEntity<UserDto> updateUser(Long userId, @Valid UserDto userDetails) {
        ResponseEntity<UserDto> response = restTemplate.exchange( getUserServiceUrl() + userId, HttpMethod.PUT, getHttpEntity(userDetails), UserDto.class);
        return verifyResponse(response, "updateUser");
        /*restTemplate.put( getUserServiceUrl() + userId, userDetails);return ResponseEntity.ok().build();*/
    }

    @Override
    public ResponseEntity issueBook(Long userId, Long bookId) {
        //TODO : get Book and User and then we can save into Library table
        Library lib = new Library();
        lib.setBookId(bookId);
        lib.setUserId(userId);
        libraryRepository.save(lib);
        return ResponseEntity.ok().build();
    }

    private HttpEntity getHttpEntity(Object request) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<>(request, headers);
    }

}
