package com.jpop.libraryservice.resource.impl;

import com.jpop.libraryservice.client.book.BookServiceClient;
import com.jpop.libraryservice.client.user.UserServiceClient;
import com.jpop.libraryservice.dto.BookDto;
import com.jpop.libraryservice.dto.UserDto;
import com.jpop.libraryservice.exception.LibraryServiceException;
import com.jpop.libraryservice.model.Library;
import com.jpop.libraryservice.repository.LibraryRepository;
import com.jpop.libraryservice.resource.LibraryResourceV2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.Valid;
import java.util.List;

@Named
public class LibraryResourceImplV2 implements LibraryResourceV2 {


    @Inject
    private LibraryRepository libraryRepository;

    @Inject
    private BookServiceClient bookServiceClient;

    @Inject
    private UserServiceClient userServiceClient;


    @Override
    public ResponseEntity<List<BookDto>> getBooks() {
        List<BookDto> response = bookServiceClient.getBooks();
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<BookDto> getBook(Long bookId) {
        BookDto response = bookServiceClient.getBook(bookId);
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<BookDto> addBook(BookDto book) {
        BookDto response = bookServiceClient.addBook(book);
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity deleteBook(Long bookId) {
        bookServiceClient.deleteBook(bookId);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<List<UserDto>> getUsers() {
        ResponseEntity<List<UserDto>> response = userServiceClient.getUsers();
        return verifyResponse(response, "getUsers");
    }

    @Override
    public ResponseEntity<UserDto> getUser(Long userId) {
        ResponseEntity<UserDto> response = userServiceClient.getUser(userId);
        return verifyResponse(response, "getUser");
    }

    @Override
    public ResponseEntity<UserDto> addUser(@Valid UserDto userDto) {
        ResponseEntity<UserDto> user = userServiceClient.addUser(userDto);
        return verifyResponse(user, "addUser");
    }

    @Override
    public ResponseEntity deleteUser(Long userId) {
        ResponseEntity<ResponseEntity> response = userServiceClient.deleteUser(userId);
        return verifyResponse(response, "deleteUser");
    }

    @Override
    public ResponseEntity<UserDto> updateUser(Long userId, @Valid UserDto userDetails) {
        ResponseEntity<UserDto> response = userServiceClient.updateUser(userId, userDetails);
        return verifyResponse(response, "updateUser");
    }

    @Override
    public ResponseEntity issueBook(Long userId, Long bookId) {
        BookDto book = bookServiceClient.getBook(bookId);
        UserDto user = userServiceClient.getUser(userId).getBody();
        Library lib = new Library();
        lib.setBookId(book.getBookId());
        lib.setUserId(user.getUserId());
        libraryRepository.save(lib);
        return ResponseEntity.ok().build();
    }

    private <T> ResponseEntity<T> verifyResponse(ResponseEntity<T> responseEntity, String param) {
        if (responseEntity.getStatusCode() == HttpStatus.OK) {
            return responseEntity;
        }
        throw new LibraryServiceException("Failed to " + param);
    }

}
