package com.jpop.libraryservice.resource;

import com.jpop.libraryservice.dto.BookDto;
import com.jpop.libraryservice.dto.UserDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/lib/v1")
@Api(tags = {"Library: Library Resource"})
public interface LibraryResource {

    @ApiOperation(value = "View list of available Books", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved Books"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @GetMapping("/books")
    ResponseEntity<List<BookDto>> getBooks();

    @ApiOperation(value = "get a book by bookId", response = BookDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved user"),
            @ApiResponse(code = 404, message = "User not found")
    })
    @GetMapping("/books/{book_id}")
    ResponseEntity<BookDto> getBook(@PathVariable(value = "book_id") Long bookId);


    @ApiOperation(value = "add a Book", response = BookDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully added a book"),
            @ApiResponse(code = 404, message = "Resource not found")
    })
    @PostMapping("/books")
    ResponseEntity<BookDto> addBook(@RequestBody BookDto book);

    @ApiOperation(value = "delete book by bookId")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully deleted book"),
            @ApiResponse(code = 404, message = "User not found")
    })
    @DeleteMapping("/books/{book_id}")
    ResponseEntity deleteBook(@PathVariable(value = "book_id") Long bookId);


    @ApiOperation(value = "View list of available users", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved users"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @GetMapping("/users")
    ResponseEntity<List<UserDto>> getUsers();

    @ApiOperation(value = "get a user by userId", response = UserDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved user"),
            @ApiResponse(code = 404, message = "User not found")
    })
    @GetMapping("/users/{user_id}")
    ResponseEntity<UserDto> getUser(@PathVariable(value = "user_id") Long userId);

    @ApiOperation(value = "create a user", response = UserDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully created a user"),
            @ApiResponse(code = 404, message = "Resource not found")
    })
    @PostMapping("/users")
    ResponseEntity<UserDto> addUser(@Valid @RequestBody UserDto UserDto);


    @ApiOperation(value = "delete user by userId")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully deleted user"),
            @ApiResponse(code = 404, message = "User not found")
    })
    @DeleteMapping("/users/{user_id}")
    ResponseEntity deleteUser(@PathVariable(value = "user_id") Long userId);


    @ApiOperation(value = "update user", response = UserDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated user"),
            @ApiResponse(code = 404, message = "User not found")
    })
    @PutMapping("/users/{user_id}")
    ResponseEntity<UserDto> updateUser(@PathVariable(value = "user_id") Long userId, @Valid @RequestBody UserDto userDetails);

    @PutMapping("/users/{user_id}/books/{book_id}")
    ResponseEntity issueBook(@PathVariable(value = "user_id") Long userId, @PathVariable(value = "book_id") Long bookId);

}
