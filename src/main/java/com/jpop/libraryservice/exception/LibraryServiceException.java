package com.jpop.libraryservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class LibraryServiceException extends RuntimeException {

    public LibraryServiceException(String msg) {
        super(msg);
    }
}
